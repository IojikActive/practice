
lst = ['12123','22','sdafasd','123','123sss','123asdasd','123sdbvsfdb','s','d','22','asdfsdfasdfasdfasdfsdfsdfs','asdfsdfasdfasdfasdfsdfsdfs','asdfsdfasdfasdfasdfsdfsdfs']


def lensort_sorted(lst:list) -> list: #  не меняет исходный список
    lst = sorted(lst,key=len)         # sorted возвращает новый список 
    return lst

def lensort_sort(lst:list) -> list: # меняет исходный список
    # r = lst.copy()
    lst.sort(key=len)
    return lst

def lensort_lambda(lst:list) -> list:
    lst = sorted(lst,key=lambda x:x)
    return lst

if __name__ == "__main__":
    print(f"global lst:{lst} \n")

    print(f"sorted:{lensort_sorted(lst)}")
    print(f"global lst:{lst} \n")

    print(f"lambda+sorted: {lensort_lambda(lst)}")
    print(f"global lst:{lst} \n")
    
    print(f"sort: {lensort_sort(lst)}")
    print(f"global lst:{lst} \n")

    