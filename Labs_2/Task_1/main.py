'''
1.	Реализовать класс вектора на плоскости. Используя перегрузку операторов, реализовать:
•	Сложение и вычитание векторов ok
•	Сравнение векторов (равно/не равно) ok
•	Умножение вектора на число ok
•	Скалярное произведение векторов
•	Получение длины вектора ok
•	Вывод вектора в консоль при помощи функции print() в формате <x; y>
•	Отображение вектора при работе с интерпретатором в формате  <x; y>

'''
from math import sqrt

class Vec:

    def __init__(self,x:float,y:float)  :
        self.x = float(x)
        self.y = float(y)
    
    def __add__(self,vec) :  # +
        vec.x = self.x + vec.x
        vec.y = self.y + vec.y
        return vec
    
    def __sub__(self,vec ) :  # -
        vec.x = self.x - vec.x
        vec.y = self.y - vec.y
        return vec

    def __eq__(self,vec ) -> bool: # == 
        if vec.x == self.x and vec.y == self.y:
            return True
        else:
            return False

    def __ne__(self,vec ) -> bool: # !=
        if vec.x == self.x and vec.y == self.y:
            return False
        else:
            return True

    def __mul__(self,num:float)  : # * на число
        self.x = self.x*num
        self.y = self.y*num
        return self

    def __len__(self): # получение длины вектора
        norm = sqrt(self.x**2 + self.y**2)
        return int(norm) # Требование для len 
        
        '''File "main.py", line 74, in <module>
            f"len(v1):{len(v1)}",
        TypeError: 'float' object cannot be interpreted as an integer'''


    def __pow__(self,vec )  : # a  * b 
        norm1 = len(self)
        norm2 = len(vec)

        cos = ()/(norm1,norm2)


        pass

    def __repr__(self): # Режим консоли
        return f"<{self.x};{self.y}>"

    def __srt__(self): # print
        return f"<{self.x};{self.y}>"


if __name__ == "__main__":

    v1 = Vec(1.0,0.0)
    v2 = Vec(10.25,0.0)
    
    print(f"v1+v2:{v1+v1}",
          f"v1-v2:{v1-v2}",
          f"v1==v2:{v1==v2}",
          f"v1!=v2:{v1!=v2}",
          f"v1*2:{v1*2}",
          f"len(v1):{len(v1)}",
          f"v1*v2: ??",
          f"print(v1):{v1}")

